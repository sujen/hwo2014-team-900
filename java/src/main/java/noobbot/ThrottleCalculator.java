package noobbot;

import java.util.ArrayList;
import java.util.List;

import noobbot.CarPositions.Data;

public class ThrottleCalculator {

    private float prevThrottle = 1.0f;
    public static float drags = 0f;
    public static double masses = 0f;
    public static float c = 0.33321125f;
    public static int x = 0;
    public static boolean once = true;
    public static boolean turbo = false;
    
    public static float calculate(List<Piece> pieces, CarPositions carPosition){
        //examine upcoming pieces
    	if(carPosition.getVelocities(0).get(0) != 0 && carPosition.getVelocities(1).get(0) != 0 && carPosition.getVelocities(2).get(0) != 0 && once)
    	{
    		calcInit(carPosition.getAllVelocities());
    		once = false;
    	}
        for(int i = 1; i < pieces.size(); i++) {
            if (pieces.get(i).getPieceType() == Piece.CURVE_PIECE) {
                //add current piece distance remaining
                double distanceToCurve;
                if(pieces.get(0).getPieceType() == Piece.CURVE_PIECE) {
                    distanceToCurve = Math.abs(pieces.get(0).getAngle()) * Math.PI / 180 * (pieces.get(0).getRadius() - GameInit.getLanes().get(carPosition.getOurCarData().getPiecePosition().getLane().getStartLaneIndex()).getDistanceFromCenter()) - carPosition.getOurCarData().getPiecePosition().getInPieceDistance();
                } else {
                    distanceToCurve = pieces.get(0).getLength() - carPosition.getOurCarData().getPiecePosition().getInPieceDistance();
                }
                for(int j = 1; j < i; j++) {
                    distanceToCurve += pieces.get(j).getLength();
                }
                return calcThrottle(carPosition.getVelocities(0).get(0), calcMaxVelocity(pieces.get(i).getRadius()),distanceToCurve);
            }
        }
        //Do Turbo if available
        if(GameInit.isTurboAvailable())
           turbo = true; 
        return 1.0f;
    }
    
    public static boolean shouldTurbo() {
        GameInit.consumeTurbo();
        return turbo; 
    }

    public static void calcInit(List<ArrayList<Float>> v)
    {		
    	// assigning all car drag values
    	drags =(v.get(2).get(0)-(v.get(1).get(0)-v.get(2).get(0)))/(v.get(2).get(0)*v.get(2).get(0)*1);
    	masses = 1.0/(Math.log((v.get(0).get(0)-(1.0/drags))/(v.get(1).get(0)-(1.0/drags)))/(-1*drags));

    }
    public static float calcMaxVelocity(double radius)
    {
    	return (float) (Math.sqrt(c*radius)+0.5);
    }
    public static float calcThrottle(float velocity,float maxVelocity, double endDistance)
    {	
    	double time =  (endDistance/((double) velocity));
    	double throttle = (maxVelocity - velocity*Math.exp((-1*drags*time)/masses))/(((-1/drags)*Math.exp((-1*drags*time)/masses))+1/drags);
    	if (throttle > 1)
    	{
    		throttle = 1.0;
    	}else if (throttle < 0)
    	{
    		throttle = 0.0;
    	}

    	return (float) throttle; 
    }
}
