package noobbot;
import java.util.List;
import java.util.ArrayList;

public class CarPositions {

    //Constants
    private static final String DOOMSTARKS_ID = "doomstarks";

	private	List<Data> data;
    private Data ourCarPos;
	private String gameId;
	private int gameTick;
	public static List <Float> prevDistances;
	private static List <ArrayList<Float>> velocities = new ArrayList<ArrayList<Float>>();
    private static float prevInPieceDistance = 0;
    public static boolean firstTime = true;

	public String getGameId() {
		return gameId;
	}
	public void setGameId(String gameId) {
		this.gameId = gameId;
	}
	public int getGameTick() {
		return gameTick;
	}
	public void setGameTick(int gameTick) {
		this.gameTick = gameTick;
	}
    public Data getOurCarData() {
        return ourCarPos;
    }
	public List<Data> getData() {
		return data;
	}
	public void setData(List <Data> data) {
		this.data = data;
	}
	
	public class Data {
		private ID id;
		private float angle;
		private PiecePosition piecePosition;
		private int lap;
		public ID getId() {
			return id;
		}
		public void setId(ID id) {
			this.id = id;
		}
		public float getAngle() {
			return angle;
		}
		public void setAngle(float angle) {
			this.angle = angle;
		}
		public PiecePosition getPiecePosition() {
			return piecePosition;
		}
		public void setPiecePosition(PiecePosition piecePosition) {
			this.piecePosition = piecePosition;
		}
		public int getLap() {
			return lap;
		}
		public void setLap(int lap) {
			this.lap = lap;
		}
	}

    public void calcVelocity ()
    {
        for(int i = 0; i < data.size(); i++)
        {
            //move preSpeed2 to preSpeed3
            velocities.set(3, new ArrayList<Float>(velocities.get(2)));
            //move preSpeed1 to preSpeed 2
            velocities.set(2, new ArrayList<Float>(velocities.get(1)));
            //move (old) currentSpeed to preSpeed 1
            velocities.set(1, new ArrayList<Float>(velocities.get(0)));

            //calculate new currentSpeed

            float currentDistance  = data.get(i).getPiecePosition().getInPieceDistance();
            float prevDistance = prevDistances.get(i);
            if(currentDistance < prevDistance) {
                velocities.get(0).set(i, currentDistance);
            } else {
                velocities.get(0).set(i, currentDistance - prevDistance);
            }
            prevDistances.set(i, currentDistance);
        }
    }

	public List<ArrayList<Float>> getAllVelocities()
	{
		return velocities;
	}

    public List<Float> getVelocities(int n) {
        return velocities.get(n);
    }
	
	public void init ()
	{
        if(firstTime) {
            for(int i = 0; i < 4; i++) {
                velocities.add(new ArrayList<Float>());
                    for(int j = 0; j < data.size(); j++) {
                        velocities.get(i).add(0f);
                    }
            }
            prevDistances = new ArrayList<Float>();
            for(int i = 0; i < data.size(); i++) {
                prevDistances.add(0f);
            }
            firstTime = false;
        } 
        //Calculate Velocities;
        calcVelocity();

        //Determine Our Car Data
        for(int i = 0; i < data.size(); i++){
            if(data.get(i).getId().getName().equalsIgnoreCase(DOOMSTARKS_ID)) {
                ourCarPos = data.get(i);
            }
        }

      

        if ( ourCarPos.getPiecePosition().getInPieceDistance() < prevInPieceDistance && gameTick > 0){
            GameInit.nextPiecePosition(); 
        }
        prevInPieceDistance = ourCarPos.getPiecePosition().getInPieceDistance();
	}
	public class ID{
		private String name;
		private String colour;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getColour() {
			return colour;
		}
		public void setColour(String colour) {
			this.colour = colour;
		}

	}
	
	public class PiecePosition{
		private int pieceIndex;
		private float inPieceDistance;
		private Lane lane;
		public int getPieceIndex() {
			return pieceIndex;
		}
		public void setPieceIndex(int pieceIndex) {
			this.pieceIndex = pieceIndex;
		}
		public float getInPieceDistance() {
			return inPieceDistance;
		}
		public void setInPieceDistance(float inPieceDistance) {
			this.inPieceDistance = inPieceDistance;
		}
		public Lane getLane() {
			return lane;
		}
		public void setLane(Lane lane) {
			this.lane = lane;
		}


	}
	
	public class Lane {
		private int startLaneIndex;
		private int endLaneIndex;
		public int getStartLaneIndex() {
			return startLaneIndex;
		}
		public void setStartLaneIndex(int startLaneIndex) {
			this.startLaneIndex = startLaneIndex;
		}
		public int getEndLaneIndex() {
			return endLaneIndex;
		}
		public void setEndLaneIndex(int endLaneIndex) {
			this.endLaneIndex = endLaneIndex;
		}
	}
	
}
